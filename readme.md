## Sensors output simulator
NodeJS app simulating the output of weather conditions sensors' values in sped-up time.

### Installation

    npm install
### Usage
    node app.js
By default simulated values are logged into a JSON file in the `out` folder and sent into a MQTT broker (default topic: `/sensors/<sensor-name>`)

### Configuration
Example config file is located in `config/default.json`. We differentiate _real_time_ (user's point of view) and _simulated_time_ (sensor's point of view)
*  `simulator`
   *  `speed`:`float` How many times faster the _simulated_time_ goes faster than the _real_time_. `24` by default, where simulated hour takes a day
   *  `interval`:`int` Sampling interval (_simulated_time_, default `60000` - once a minute)
   *  `sensors` list of simulated sensors in format`name:attributesObj`, where attributes are:
      *  `minValue`:`float` Minimum possible sensor value
      *  `minHour`:`float` An hour when the minimum value occurs
      *  `maxValue`:`float`  Maximum possible sensor value
      *  `maxHour`:`float` An hour when the maximum value occurs
      *  `maxDeviation`:`float` Maximum deviation from the value lying on the interpolation function 
      *  `precision`:`int` Decimal number precision of generated values
*  `mqtt`
   *  `broker`:`string` URL of the MQTT broker
   *  `eventTopic`:`string` Topic for sending the generated values (default `/sensors/`)
*  `output`
   *  `folder`:`string` Filepath for the output JSON files (will be created if does not exist, default `out/`)
   *  `consoleEnabled`:`bool` Enable std out logging (default `false`)

### Notes
*  The simulation works by interpolating the given minimums and maximums during the day and adding a random deviation inside the given interval
*  Library [accurate-interval](https://www.npmjs.com/package/accurate-interval) had been used as `setInterval` does not guarantee accuracy.

### TO-DO
*  Config validation
*  Unit tests
*  Universal solution for more than 2 points for interpolation